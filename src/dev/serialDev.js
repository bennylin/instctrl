/* eslint class-methods-use-this: "off" */
/* eslint prefer-destructuring: "off" */
// const fs = require('fs');
import devSpec from '../spec/supportedDev.json';
import pwsSCPI from '../spec/pwsSCPI.json';

const SerialPort = require('serialport');


// const pwrModel = ['PFR'];

const supportedPort = [
  {
    path: '/dev/ttyACM0',
    baudRate: 9600,
  },
  {
    path: '/dev/ttyACM1',
    baudRate: 9600,
  }];

// const devIdn = [];

const devSearchedList = [];

class DevObj {
  constructor(devName, baudRate) {
    this.port = new SerialPort(devName, { baudRate, autoOpen: false });
    this.dataHandler = this.dataHandler.bind(this);
    this.errEventHandler = this.errEventHandler.bind(this);
    this.sendCmd = this.sendCmd.bind(this);
    this.flushDev = this.flushDev.bind(this);
    this.cmdHandler = null;
    this.errHandler = null;
    this.model = '';
    this.serialNum = '';
    this.id = '';
    this.cmdDone = false;
    this.recData = '';
    this.scpi = {};
  }

  dataHandler(data) {
    this.recData += data.slice();
    if (data[data.length - 1] === 0x0a) {
      if (typeof this.cmdHandler === 'function') {
        this.cmdHandler(null, this.recData);
      }
    }
  }

  errEventHandler(err) {
    if (typeof this.errHandler === 'function') {
      this.errHandler(err);
    }
  }

  sendCmd(cmd, done) {
    this.port.open((err) => {
      if (err) {
        console.log(`open usb dev error ${err}`);
      }

      this.port.write(cmd, (error) => {
        if (error) {
          console.log(`write usb dev error ${err}`);
          this.errEventHandler(err);
        }
        console.log('message written');
        if (typeof done === 'function') {
          this.port.drain(done);
        }
      });
    });
  }

  flushDev(done) {
    this.cmdDone = false;
    this.recData = '';
    this.port.flush(done);
  }
}

function devOpen(port, dataHandler, errHandler) {
  port.on('data', (data) => {
    console.log(`usb dev data ${data.toString()}`);
    dataHandler(data);
  });

  port.on('error', (err) => {
    errHandler(err);
  });

  port.open((err) => {
    if (err) {
      console.log(`open usb dev error ${err}`);
      return;
    }

    port.write('*idn?\r\n', (error) => {
      if (error) {
        console.log(`write usb dev error ${err}`);
      }
      console.log('message written');
    });
  });
}

function delInstObj(did) {
  const index = devSearchedList.findIndex(obj => obj.id === did);

  if (index >= 0) {
    devSearchedList.splice(index, 1);
  }
}

function getID(devName, baudRate) {
  if (devSearchedList.find(obj => obj.devPath === devName)) {
    return Promise.resolve();
  }

  return new Promise((resolve) => {
    const instObj = new DevObj(devName, baudRate);
    const errTimer = setTimeout(resolve, 2000);
    let spec;

    instObj.cmdHandler = function (err, data) {
      instObj.port.close();
      if (err) {
        this.recData = '';
        return;
      }
      this.model = data.toString().split(',')[1];
      this.serialNum = data.toString().split(',')[2];

      spec = devSpec[this.model];
      if (spec) {
        this.id = `${this.model.split('-')[0]}-${this.serialNum}`;
        this.devPath = this.port.path;
        this.recData = '';
        this.spec = spec;
        if (pwsSCPI[spec.cmdID]) {
          this.scpi = pwsSCPI[spec.cmdID];
        }

        devSearchedList.push(instObj);
      }
      clearTimeout(errTimer);
      resolve();
    };

    instObj.errHandler = function (err) {
      console.log(`catch0 error ${err}`);
      clearTimeout(errTimer);
      resolve();
    };

    devOpen(instObj.port, instObj.dataHandler, instObj.errEventHandler);
  });
}

function getQueryData(did, cmd) {
  return new Promise((resolve, reject) => {
    const instObj = devSearchedList.find(obj => obj.id === did);

    if (instObj) {
      instObj.cmdHandler = function (err, data) {
        instObj.port.close();
        if (err) {
          this.recData = '';
          delInstObj(did);
          reject(err);
        } else {
          this.recData = '';
          resolve(data.toString());
        }
      };

      instObj.errHandler = function (err) {
        console.log(`catch1 error ${err}`);
        delInstObj(did);
        reject(err);
      };

      instObj.flushDev(() => {
        instObj.sendCmd(cmd);
      });
    } else {
      reject(new Error('device not exist'));
    }
  });
}

function sendCommand(did, cmd) {
  return new Promise((resolve, reject) => {
    const instObj = devSearchedList.find(obj => obj.id === did);

    instObj.cmdHandler = null;
    if (instObj) {
      instObj.errHandler = function (err) {
        console.log(`catch error ${err}`);
        delInstObj(did);
        reject(err);
      };

      console.log(`send command: ${cmd}`);
      instObj.flushDev(() => {
        instObj.sendCmd(cmd, resolve);
      });
    } else {
      reject(new Error('device not exist'));
    }
  });
}

function getDevObj(did) {
  const devObj = devSearchedList.find(obj => obj.id === did);

  if (devObj) {
    return devObj;
  }
  return null;
}

function serialDevSearch() {
  return new Promise((resolve) => {
    const pl = supportedPort.map(dev => getID(dev.path, dev.baudRate));

    Promise.all(pl).then(() => {
      resolve(devSearchedList);
    }).catch(() => {
      resolve(devSearchedList);
    });
  });
}

export default serialDevSearch;
export { getQueryData, getDevObj, sendCommand };
