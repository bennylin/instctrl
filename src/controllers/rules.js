function specCheckRated(devObj, cmdType, val, compare) {
  const fVal = parseFloat(val);

  if (devObj === null) {
    console.log('devObj not exist');
    return null;
  }

  if (devObj.scpi[cmdType]) {
    if (Number.isNaN(fVal)) {
      console.log('not a number');
      if (devObj.scpi[cmdType].paraType.search('string') >= 0) {
        if (devObj.scpi[cmdType].parameter.find(e => e === val)) {
          return `${devObj.scpi[cmdType].cmd} ${val}`;
        }
      }
      return null;
    }

    if (fVal > compare * devObj.scpi[cmdType].maxima) {
      console.log('number bigger then compare');
      return null;
    }

    if (fVal < compare * devObj.scpi[cmdType].minima) {
      console.log('number smaller then compare');
      return null;
    }

    return `${devObj.scpi[cmdType].cmd} ${val}`;
  }
  console.log('scpi command not exist');
  return null;
}

function specCheckString(devObj, cmdType, val) {
  if (devObj === null) {
    console.log('devObj not exist');
    return null;
  }

  if (devObj.scpi[cmdType]) {
    if (devObj.scpi[cmdType].paraType.search('string') >= 0) {
      if (devObj.scpi[cmdType].parameter.find(e => e === val)) {
        return `${devObj.scpi[cmdType].cmd} ${val}`;
      }
    }
    return null;
  }
  console.log('scpi command not exist');
  return null;
}

export default specCheckRated;
export { specCheckString };
