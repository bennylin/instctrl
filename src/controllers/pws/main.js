import fs from 'fs';
import serialDevSearch, { getQueryData, getDevObj, sendCommand } from '../../dev/serialDev';
import specCheckRated, { specCheckString } from '../rules';
import errMessage from '../../spec/errMessage.json';

const MAX_CONFIG_LENGTH = 10;
const PWS_CONFILE_PATH = './pwsConfig.json';
let outputConfig = null;

function pwsSearchDev(req, res) {
  if (outputConfig === null) {
    if (fs.existsSync(PWS_CONFILE_PATH)) {
      outputConfig = JSON.parse(fs.readFileSync(PWS_CONFILE_PATH));
    } else {
      outputConfig = {};
    }
  }
  serialDevSearch().then((devList) => {
    const idlist = devList.map(obj => obj.id);
    res.status(200).send(idlist);
  }).catch((err) => {
    res.status(400).send(` command not supported, ${err}`);
  });
}

function GetOutputConfig(req, res) {
  const devObj = getDevObj(req.params.dev_id);

  if (devObj === null) {
    res.status(403).send(errMessage['403']);
    return;
  }

  if (req.params.conf_id > MAX_CONFIG_LENGTH) {
    res.status(403).send(errMessage['403']);
    return;
  }

  if (outputConfig[devObj.id]) {
    const config = Object.assign({}, outputConfig[devObj.id][req.params.conf_id]);
    if (config) {
      delete config.cmd;
      res.status(200).send(config);
    } else {
      res.status(404).send(errMessage['404']);
    }
  } else {
    res.status(403).send(errMessage['403']);
  }
}

function SetOutputConfig(req, res) {
  const devObj = getDevObj(req.params.dev_id);
  let cmd = '';
  let state = '';

  if (devObj === null) {
    res.status(403).send(errMessage['403']);
    return;
  }

  if (req.params.conf_id > MAX_CONFIG_LENGTH) {
    res.status(403).send(errMessage['403']);
    return;
  }

  if (req.body.vout) {
    state = specCheckRated(devObj, 'volt', req.body.vout, devObj.spec.vMax);
    if (state === null) {
      res.status(403).send(errMessage['403']);
      return;
    }
    cmd += state;
  }

  if (req.body.vlimit) {
    if (req.body.ilimit === 0) {
      res.status(403).send(errMessage['403']);
      return;
    }
    state = specCheckRated(devObj, 'voltOVL', req.body.vlimit, devObj.spec.vMax);
    if (state === null) {
      res.status(403).send(errMessage['403']);
      return;
    }
    cmd = `${cmd};${state}`;
  }

  if (req.body.iout) {
    state = specCheckRated(devObj, 'curr', req.body.iout, devObj.spec.iMax);
    if (state === null) {
      res.status(403).send(errMessage['403']);
      return;
    }
    cmd = `${cmd};${state}`;
  }

  if (req.body.ilimit) {
    if (req.body.ilimit === 0) {
      res.status(403).send(errMessage['403']);
      return;
    }

    state = specCheckRated(devObj, 'voltOCL', req.body.ilimit, devObj.spec.iMax);
    if (state === null) {
      res.status(403).send(errMessage['403']);
      return;
    }
    cmd = `${cmd};${state}`;
  }

  if (req.body.mode) {
    state = specCheckString(devObj, 'outputMode', req.body.mode);
    if (state === null) {
      res.status(403).send(errMessage['403']);
      return;
    }
    cmd = `${cmd};${state}`;
  }

  // console.log(devObj);
  console.log(req.body);
  // console.log(cmd);
  if (outputConfig[devObj.id]) {
    outputConfig[devObj.id][req.params.conf_id] = Object.assign({}, req.body, { cmd });
  } else {
    outputConfig[devObj.id] = new Array(MAX_CONFIG_LENGTH + 1);
    outputConfig[devObj.id][req.params.conf_id] = Object.assign({}, req.body, { cmd });
  }
  console.log(outputConfig);
  fs.writeFileSync(PWS_CONFILE_PATH, JSON.stringify(outputConfig));
  res.status(200).send('OK');
}

function EnableOutput(req, res) {
  const devObj = getDevObj(req.params.dev_id);

  if (devObj === null) {
    res.status(403).send(errMessage['403']);
    return;
  }

  if (req.params.conf_id > MAX_CONFIG_LENGTH) {
    res.status(403).send(errMessage['403']);
    return;
  }

  if (outputConfig[devObj.id]) {
    if (outputConfig[devObj.id][req.params.conf_id].cmd) {
      const state = specCheckString(devObj, 'outputState', req.body.oe);
      if (state === null) {
        res.status(403).send(errMessage['403']);
      } else {
        sendCommand(
          devObj.id,
          `${outputConfig[devObj.id][req.params.conf_id].cmd};${state}\r\n`,
        ).then(() => {
          res.status(200).send('OK');
        }).catch(() => {
          res.status(403).send(errMessage['403']);
        });
      }
    } else {
      res.status(404).send(errMessage['404']);
    }
  } else {
    res.status(403).send(errMessage['403']);
  }
}

function GetQueryData(req, res) {
  getQueryData(req.params.dev_id, `${req.body.cmd}\r\n`).then((vol) => {
    res.status(200).send(vol);
  }).catch((err) => {
    console.log(err);
    res.status(404).send(err);
  });
}
function GetMeasurement(req, res) {
  let cmd = '';

  if (req.body.type === 'vout') {
    cmd = ':MEAS:VOLT?\r\n';
  } else if (req.body.type === 'iout') {
    cmd = ':MEAS:CURR?\r\n';
  } else if (req.body.type === 'power') {
    cmd = ':MEAS:POW?\r\n';
  } else {
    res.status(400).send('command type not supported');
    return;
  }

  getQueryData(req.params.dev_id, cmd).then((vol) => {
    res.status(200).send(vol);
  }).catch((err) => {
    console.log(err);
    res.status(404).send(err);
  });
}

export default pwsSearchDev;
export { GetOutputConfig, SetOutputConfig, EnableOutput, GetMeasurement, GetQueryData };

